#!/bin/bash
# shellcheck disable=SC2034,SC2154  # variable appears unused/is referenced but not assigned
# shellcheck disable=SC2312         # masking of return values

eval "$(shellspec - -c) exit 1"

Describe 'array support'
    Context 'array ( foo bar baz )'
        arr=( foo bar baz )
        It 'is supported by the array subject'
            When call :
            The array arr should eq "foo"  # first element in Bash
        End

        It 'is supported by the element modifier'
            When call :
            The element 0 of array arr should eq "foo"
            The element 1 of array arr should eq "bar"
            The element 1 of array arr should not eq "notbar"
            The element 2 of array arr should eq "baz"
            The element 3 of array arr should be undefined
        End

        It 'is supported by the values matcher'
            When call :
            The array arr should be values "${arr[@]}"
            The array arr should be values "foo" "bar" "baz"
            The array arr should not be values "foo bar baz"
        End

        It 'is supported by the empty matcher'
            When call :
            The array arr should not be empty
        End
    End

    Context 'empty array'
        arr=()

        # empty arrays appear undefined and are empty

        It 'is supported by the array subject'
            When call :
            The array arr should be undefined  # this is a weird corner case
        End

        It 'is supported by the element modifier'
            When call :
            The element 0 of array arr should be undefined
        End

        It 'is supported by the empty matcher'
            When call :
            The array arr should be empty
        End
    End

    Context 'unset array'
        unset arr

        # unset arrays are undefined and appear empty

        It 'is supported by the array subject'
            When call :
            The array arr should be undefined
            The array arr should not eq ""
        End

        It 'is supported by the element modifier'
            When call :
            The element 0 of array arr should be undefined
        End

        It 'is supported by the empty matcher'
            When call :
            The array arr should be empty  # this is a weird corner case
        End
    End
End

---
# gitlab-yaml-shellcheck: predefined=CI_API_V4_URL,CI_COMMIT_REF_NAME
# gitlab-yaml-shellcheck: predefined=CI_JOB_ID,CI_JOB_NAME,CI_JOB_STAGE,CI_JOB_STATUS,CI_JOB_TOKEN,CI_JOB_URL
# gitlab-yaml-shellcheck: predefined=CI_PIPELINE_ID,CI_PIPELINE_URL,CI_PROJECT_DIR,CI_PROJECT_ID
# gitlab-yaml-shellcheck: predefined=architectures,brew_task_id,kernel_version
# gitlab-yaml-shellcheck: predefined=make_target,merge_branch,name,patch_urls,rpm_release,server_url
# gitlab-yaml-shellcheck: predefined=status,target_branch,target_repo,test_runner,top_url,tree_name
# gitlab-yaml-shellcheck: predefined=web_url,original_pipeline,mr_pipeline_id,native_architectures

include:
  - pipeline/common-before-after.yml
  - pipeline/constraints.yml
  - pipeline/rules.yml
  - pipeline/stages.yml
  - pipeline/variables.yml
  - pipeline/functions/*.yml
  - pipeline/stages/*.yml
  # https://docs.gitlab.com/ee/ci/yaml/includes.html#use-variables-with-include
  # the global variables section is NOT taken into account in include if
  # expressions! These variables will appear as undefined unless overridden via
  # trigger variables, which is checked below by comparing to null.
  - {local: pipeline/dependencies/publish-with-native-tools-ppc64le.yml, rules: [
    {if: '$native_tools =~ /^[Tt]rue$/ && $native_architectures !~ /.*\bppc64le\b.*/ && ($native_tools_architectures == null || $native_tools_architectures =~ /.*\bppc64le\b.*/)'}]}
  - {local: pipeline/dependencies/publish-with-native-tools-aarch64.yml, rules: [
    {if: '$native_tools =~ /^[Tt]rue$/ && $native_architectures != null && $native_architectures !~ /.*\baarch64\b.*/ && ($native_tools_architectures == null || $native_tools_architectures =~ /.*\baarch64\b.*/)'}]}
  - {local: pipeline/dependencies/publish-with-native-tools-s390x.yml, rules: [
    {if: '$native_tools =~ /^[Tt]rue$/ && $native_architectures !~ /.*\bs390x\b.*/ && ($native_tools_architectures == null || $native_tools_architectures =~ /.*\bs390x\b.*/)'}]}
  - {local: pipeline/dependencies/publish-with-native-tools-riscv64.yml, rules: [
    {if: '$native_tools =~ /^[Tt]rue$/ && $native_architectures !~ /.*\briscv64\b.*/ && $native_tools_architectures =~ /.*\briscv64\b.*/'}]}
  - {local: pipeline/dependencies/publish-without-native-tools-ppc64le.yml, rules: [
    {if: '$native_tools !~ /^[Tt]rue$/ || $native_architectures =~ /.*\bppc64le\b.*/ || ($native_tools_architectures != null && $native_tools_architectures !~ /.*\bppc64le\b.*/)'}]}
  - {local: pipeline/dependencies/publish-without-native-tools-aarch64.yml, rules: [
    {if: '$native_tools !~ /^[Tt]rue$/ || $native_architectures == null || $native_architectures =~ /.*\baarch64\b.*/ || ($native_tools_architectures != null && $native_tools_architectures !~ /.*\baarch64\b.*/)'}]}
  - {local: pipeline/dependencies/publish-without-native-tools-s390x.yml, rules: [
    {if: '$native_tools !~ /^[Tt]rue$/ || $native_architectures =~ /.*\bs390x\b.*/ || ($native_tools_architectures != null && $native_tools_architectures !~ /.*\bs390x\b.*/)'}]}
  - {local: pipeline/dependencies/publish-without-native-tools-riscv64.yml, rules: [
    {if: '$native_tools !~ /^[Tt]rue$/ || $native_architectures =~ /.*\briscv64\b.*/ || $native_tools_architectures !~ /.*\briscv64\b.*/'}]}
  - {local: pipeline/dependencies/build-native-ppc64le.yml, rules: [
    {if: '$native_architectures =~ /.*\bppc64le\b.*/'}]}
  - {local: pipeline/dependencies/build-cross-ppc64le.yml, rules: [
    {if: '$native_architectures !~ /.*\bppc64le\b.*/'}]}
  - {local: pipeline/dependencies/build-native-aarch64.yml, rules: [
    {if: '$native_architectures == null || $native_architectures =~ /.*\baarch64\b.*/'}]}
  - {local: pipeline/dependencies/build-cross-aarch64.yml, rules: [
    {if: '$native_architectures != null && $native_architectures !~ /.*\baarch64\b.*/'}]}
  - {local: pipeline/dependencies/build-native-s390x.yml, rules: [
    {if: '$native_architectures =~ /.*\bs390x\b.*/'}]}
  - {local: pipeline/dependencies/build-cross-s390x.yml, rules: [
    {if: '$native_architectures !~ /.*\bs390x\b.*/'}]}
  - {local: pipeline/dependencies/build-native-riscv64.yml, rules: [
    {if: '$native_architectures =~ /.*\briscv64\b.*/'}]}
  - {local: pipeline/dependencies/build-cross-riscv64.yml, rules: [
    {if: '$native_architectures !~ /.*\briscv64\b.*/'}]}
  - {local: pipeline/dependencies/tag-production-prefix.yml, rules: [
    {if: '$CKI_DEPLOYMENT_ENVIRONMENT == null || $CKI_DEPLOYMENT_ENVIRONMENT =~ /^production/'}]}
  - {local: pipeline/dependencies/tag-non-production-prefix.yml, rules: [
    {if: '$CKI_DEPLOYMENT_ENVIRONMENT != null && $CKI_DEPLOYMENT_ENVIRONMENT !~ /^production/'}]}

workflow:
  name: $PIPELINE_NAME
  rules:
    # kernel MRs
    - if: $CI_PIPELINE_SOURCE == "pipeline"
      variables:
        PIPELINE_NAME: '$trigger_job_name for $mr_project_path!$mr_id: $mr_title $pipeline_name_suffix'
    # gitlab-ci-bot/retrigger/koji-trigger/gitrepo-trigger
    - if: $CI_PIPELINE_SOURCE == "api"
      variables:
        PIPELINE_NAME: '$title'

.with_artifacts:
  artifacts:
    when: always
    paths:
      - artifacts-meta.json
      - envvars.json  # ENVVARS_DUMPFILE_NAME
      - kcidb_all.json
    expire_in: 6 months

.with_retries:
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
      - unknown_failure
      - api_failure

.with_timeout:
  timeout: 5h

.with_python_image:
  image: quay.io/cki/python:${python_image_tag}

.with_builder_image:
  image: ${builder_image}:${builder_image_tag}
